import gym
import numpy as np


def print_status(guess, reward, no_and_guess_count_dict):
    print("---")
    print(
        "Guess : ",
        guess,
        " | Reward : ",
        reward,
        " | Number : ",
        no_and_guess_count_dict["number"],
        " | Guess Count : ",
        no_and_guess_count_dict["guesses"],
    )


env = gym.make("GuessingGame-v0")
env.reset()
guess = 300
l_lim, u_lim = None, None
try:
    observation, reward, done, no_and_guess_count_dict = env.step(np.array([guess]))
    print_status(guess, reward, no_and_guess_count_dict)
    while not done:
        if observation == 1:
            l_lim = guess
        if observation == 3:
            u_lim = guess
        if l_lim is None:
            guess = u_lim - 1000
        elif u_lim is None:
            guess = l_lim + 1000
        else:
            guess = (l_lim + u_lim) / 2
        observation, reward, done, no_and_guess_count_dict = env.step(np.array([guess]))
        print_status(guess, reward, no_and_guess_count_dict)
except AssertionError:
    print("Initial guess out of range")
