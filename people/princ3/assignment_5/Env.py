import numpy as np
import yaml
import sys, os
import itertools as it
import time


class Environment:
    def __init__(self):

        self.objective = """
                         +----------------------------------+
                         |             OBJECTIVE            |
                         +----------------------------------+
                         | You begin from the lower most    |
                         | side walk. You have to reach the |
                         | upper most sidewalk to win       |
                         +----------------------------------+
                         """
        self.rules = """
                         +-------------------------------+
                         |    THE RULES OF THE GAME      |
                         +-------------------------------+
                         | $ You can move up,down,left,  |
                         |   right or stay put!          |
                         | $ If you move to a non empty  |
                         |   cell you die                |
                         | $ The road has toroidal       |
                         |   boundary for the agent. ie, |
                         |   if you cross right boundary |
                         |   you will reach left.        |
                         | $ After many trials whoever   |
                         |   has most no of successes    |
                         |   WINS !!!                    |
                         +-------------------------------+
                     """
        self.help = """
                         +-------------+
                         | VALID MOVES |
                         +-------------+
                         | U = UP      |
                         | D = DOWN    |
                         | L = LEFT    |
                         | R = RIGHT   |
                         | O = STAY    |
                         +-------------+
                     """
        print(self.objective + self.rules + self.help)
        input("Press ENTER to begin !!!")
        self.config = yaml.safe_load(open("config.yaml"))["env"]
        self.no_of_lanes = self.config["no_of_lanes"]
        self.len_of_road = self.config["len_of_road"]
        self.lane_velocity = self.config["lane_velocity"]
        self.list_of_vehicles = self.config["list_of_vehicles"] + str("-- " * 50).split(
            " "
        )
        self.list_of_vehicles = self.list_of_vehicles[:-1]
        self.road = []
        self.side_walks = self.config["side_walks"]
        self.vel = []
        for i in range(self.no_of_lanes):
            self.vel.append(self.side_walks[i])
            self.vel.append(self.lane_velocity[i])
        self.vel = self.vel + self.side_walks[-1:]
        self.counter = 0
        self.reset_road()
        self.neighbor_lanes = []
        for i in range(len(self.vel)):
            if i % 2 == 0:
                if self.vel[i] == 1:
                    self.neighbor_lanes.append(i)
            else:
                self.neighbor_lanes.append(i)

        self.available_pos = []
        self.av_agent_pos()
        print(self.available_pos)
        print(f"Only {len(self.available_pos)} agents can be accomodated at max !")
        self.agents = {}
        self.ALL_DEAD = False
        self.no_of_dead = 0
        self.no_of_crossed = 0

    def get_road(self):

        return self.road

    def advance_lane(self, lane, speed):
        if speed < 0:
            new_lane = (
                lane.tolist()
                + np.random.choice(self.list_of_vehicles, size=abs(speed)).tolist()
            )
            new_lane = new_lane[-self.len_of_road :]
        else:
            new_lane = (
                np.random.choice(self.list_of_vehicles, size=abs(speed)).tolist()
                + lane.tolist()
            )
            new_lane = new_lane[: self.len_of_road]

        return np.asarray(new_lane)

    def advance_road(self, rd, vis=False):
        self.counter += 1
        for i in range(2 * self.no_of_lanes + 1):
            if i % 2 != 0:
                rd[i] = self.advance_lane(rd[i], self.vel[i])
        if vis:
            self.visualize()
        return rd

    def visualize(self, rd):
        rd = self.rub_in_agents(rd)
        os.system("clear")
        print("no of advances : ", self.counter)
        print("dead : ", self.no_of_dead)
        print("crossed : ", self.no_of_crossed)
        alist = rd.tolist()
        rd = "\n".join(" ".join(i) for i in alist)
        print(rd)
        time.sleep(1)

    def reset_road(self):
        self.road = np.random.choice(
            ["--"], size=(2 * self.no_of_lanes + 1, self.len_of_road)
        )
        rd = np.copy(self.road)
        for i in range(2 * self.len_of_road):
            self.road = self.advance_road(rd)
        self.counter = 0

    def av_agent_pos(self):
        x = np.arange(self.len_of_road)
        y = []
        for i in range(len(self.vel)):
            if i % 2 == 0:
                if self.vel[i] == 1:
                    y.append(i)
        self.available_pos = list(it.product(y[-1:], x))

    def new_agent_pos(self):
        assert self.available_pos != [], "No more side walks to accomodate agents"
        idx = np.random.choice(np.arange(len(self.available_pos)))
        pos = self.available_pos.pop(idx)
        self.road[pos[0], pos[1]] = self.config["agent"]
        return pos

    def reset_agents(self):
        self.av_agent_pos()
        for idx, agent in self.agents.items():
            agent["pos"] = self.new_agent_pos()
            agent["alive"] = True
            agent["crossed"] = False
            agent["dead"] = False
        self.ALL_DEAD = False
        self.no_of_dead = 0
        self.no_of_crossed = 0

    def new_pos(self, move, agent, rd):
        current_lane = agent["pos"][0]
        idy = self.neighbor_lanes.index(current_lane)
        idx = agent["pos"][1]
        old_pos = (current_lane, idx)
        if move == "U":
            idy -= 1
        elif move == "D":
            if current_lane != 2 * self.no_of_lanes:
                idy += 1
        elif move == "L":
            idx -= 1
        elif move == "R":
            idx += 1
        else:
            pass

        if idy == len(self.neighbor_lanes):
            idy = 0
        if idx == self.len_of_road:
            idx = 0
        if idx == -1:
            idx = self.len_of_road - 1
        if idy == -1:
            idy = len(self.neighbor_lanes) - 1

        new_lane, x = self.neighbor_lanes[idy], idx
        print(current_lane, new_lane, move, rd[new_lane, x])
        if rd[new_lane, x] == "--":
            # print("SAFE")
            # input()
            return True, (new_lane, x), old_pos
        else:
            # print("NOT SAFE")
            # input()
            return False, agent["pos"], old_pos

    def move_it(self, move, agent, rd):

        valid_moves = ["U", "D", "L", "R", "O"]
        assert move in valid_moves, (
            "\n\n Your move is not valid. Choose a valid move !!!\n" + self.help
        )
        possible, new_pos, old_pos = self.new_pos(move, agent, rd)
        if possible:
            agent["pos"] = new_pos
            rd[new_pos[0], new_pos[1]] = self.config["agent"]
        else:
            agent["alive"] = False
            agent["dead"] = True
            self.no_of_dead += 1

        if new_pos[0] == 0:
            agent["crossed"] = True
            self.no_of_crossed += 1
            agent["success"] += 1
        return agent, rd

    def rub_off_agents(self, rd):
        for i in range(len(self.vel)):
            for j in range(self.len_of_road):
                if rd[i, j] == self.config["agent"]:
                    rd[i, j] = "--"
        return rd

    def rub_in_agents(self, rd):
        for name, agent in self.agents.items():
            if not agent["dead"]:
                pos = agent["pos"]
                rd[pos[0], pos[1]] = self.config["agent"]
        return rd

    def step(self, trial):
        dead_count = []
        rd = self.advance_road(self.road)
        rd = self.rub_off_agents(rd)
        for name, agent in self.agents.items():
            if not agent["crossed"]:
                if agent["alive"]:
                    self.road = self.rub_in_agents(self.road)
                    move = agent["func"](self.road, agent["pos"], self.vel, name)
                    self.agents[name], rd = self.move_it(move, agent, rd)
                    self.road = self.rub_off_agents(self.road)
                    dead_count.append(self.agents[name]["dead"])
        self.road = rd
        self.visualize(self.road)

        if np.all(dead_count):
            self.ALL_DEAD = True
